# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Translators:
# Ivan <personal@live.hk>, 2020
msgid ""
msgstr ""
"Project-Id-Version: antix-development\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-06-03 21:05+0300\n"
"PO-Revision-Date: 2014-06-03 18:57+0000\n"
"Last-Translator: Ivan <personal@live.hk>, 2020\n"
"Language-Team: Chinese (Taiwan) (http://www.transifex.com/anticapitalista/antix-development/language/zh_TW/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: zh_TW\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.4\n"

#: desktop-session-exit:80
msgid "Lock Screen"
msgstr "鎖屏"

#: desktop-session-exit:81
msgid "Hibernate"
msgstr "休眠"

#: desktop-session-exit:82
msgid "Reboot"
msgstr "重啟"

#: desktop-session-exit:84
msgid "Log Out"
msgstr "登出"

#: desktop-session-exit:85
msgid "Suspend"
msgstr "暫停"

#: desktop-session-exit:86
msgid "Shutdown"
msgstr "關機"

#: desktop-session-exit:88
msgid "Restart Session"
msgstr "重新開始會話"
