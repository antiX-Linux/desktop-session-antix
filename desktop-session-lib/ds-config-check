#!/bin/bash
#Name: ds-config-check
#Version: 1.0
#Depends: 
#Author: Dave (david@daveserver.info)
#Purpose: Check integrity of desktop-session.conf
#License: gplv2

#Load file mapings
. /usr/local/lib/desktop-session/desktop-session-file-locations.sh

TEXTDOMAINDIR=/usr/share/locale 
TEXTDOMAIN=desktop-session

if [ ! -f $user_config ]; then
    mkdir -p $user_dts_dir;
    touch $user_config
    cat <<EOT >> $user_config
#This is the desktop-session config file.
#This is structured in a bash script format, so all options must be option="desired-option
EOT
fi 

if ! grep --quiet "^STARTUP_DELAY=" $user_config; then 
    cat <<EOT >> $user_config

#Session service delay.
#This is to set how long to delay startup to give enough time for the wm to completely load.
#before starting to load other applications. This wait period will be started after the 
#window manager has been found running.
#TIME is expressed in seconds
#Options: 0-9...
STARTUP_DELAY="2"
EOT
fi

if ! grep --quiet "^TIMES_TO_CHECK=" $user_config; then 
    cat <<EOT >> $user_config

#Session window manager check
#This is to set the amount of times desktop-session checks for a running window manager
#before giving up and denying the session to load.
#Options: 0-9...
TIMES_TO_CHECK="20"
EOT
fi

if ! grep --quiet "^FORCE_LOAD=" $user_config; then 
    cat <<EOT >> $user_config

#Session give up override
#This is to set the override if the window manager is never found to be running by 
#desktop-session. This will force the session to load if set to true.
#Options: true | false
FORCE_LOAD="false"
EOT
fi

if ! grep --quiet "^SESSION_PROTECT=" $user_config; then 
    cat <<EOT >> $user_config

#Session Protect.
#This is how to protect the user from automatic updating of any file within desktop-session.
#Examples are the menu, any file compare within file_compare, etc
#Options for this are: 
#SESSION_PROTECT="true #Strictly protect the user, leave the user to deal with any of the updating
#SESSION_PROTECT="ask #Notify me at startup of any changes and allow me to choose what to update
#SESSION_PROTECT="false #Just update me, I dont want to worry about falling behind.
SESSION_PROTECT="false"
EOT
fi

if ! grep --quiet "^OTHER_DESKTOPS_WINDOW=" $user_config; then 
    cat <<EOT >> $user_config

#Session other desktops window
#This is to set the other desktops window to pop up or not when a non default desktop is started.
#true = pop up
#false = no pop up
OTHER_DESKTOPS_WINDOW="true"
EOT
fi

if ! grep --quiet "^NOTIFICATION_DIALOG=" $user_config; then 
    cat <<EOT >> $user_config

#Notification Dialog
#This is where to enable / disable the session loading dialog
#Options: true | false
NOTIFICATION_DIALOG="false"
EOT
fi

if ! grep --quiet "^NOTIFICATION_TEXT=" $user_config; then 
    cat <<EOT >> $user_config

#Notification text or program
#This is where the text is set for the notification that the session is loading.
#This can be a specified text string or a program that will return a text string
#Examples:
#NOTIFICATION_TEXT="Session is Loading, Please Wait....."
#NOTIFICATION_TEXT="/usr/games/fortune"
NOTIFICATION_TEXT="Session is Loading, Please Wait....."
EOT
fi

if ! grep --quiet "^STARTUP_DIALOG=" $user_config; then 
    cat <<EOT >> $user_config

#Startup Dialog
#This is where to enable / disable the session startup dialog
#Options: true | false
STARTUP_DIALOG="false"
EOT
fi

if ! grep --quiet "^STARTUP_DIALOG_CMD=" $user_config; then 
    cat <<EOT >> $user_config

#Startup Dialog Command
#This is where to set the startup dialog command
#Examples: 
#STARTUP_DIALOG_CMD="leafpad ~/.startup-text
#STARTUP_DIALOG_CMD="yad --image='info' --text='this is my startup dialog'"
STARTUP_DIALOG_CMD="/usr/local/lib/desktop-session/startup-dialog.py"
EOT
fi

if ! grep --quiet "^STARTUP_SOUND=" $user_config; then 
    cat <<EOT >> $user_config

#Startup Sound
#Play Sound when loading the desktop (uses terminal command play)
#Options: true | false
STARTUP_SOUND="false"
EOT
fi

if ! grep --quiet "^STARTUP_SOUND_LEVEL=" $user_config; then 
    cat <<EOT >> $user_config

#Default Sound Level
#Set the sound volume level when starting up to default level when startup sound is true
#Set using amixer sset 'Master' -M
#Level is set from 0% - 100%
STARTUP_SOUND_LEVEL="25%"
EOT
fi

if ! grep --quiet "^STARTUP_SOUND_FILE=" $user_config; then 
    cat <<EOT >> $user_config

#Startup Sound File
#Location of the file for the startup sound 
#Can be left blank if you would like to set default sound level but not have a startup sound
STARTUP_SOUND_FILE="$HOME/Music/startup.ogg"
EOT
fi

if ! grep --quiet "^LOAD_CONKY=" $user_config; then 
    cat <<EOT >> $user_config

#Session load conky
#This will load a window manager specific conky from ~/.conky
#There must be a corresponding conkyrc in ~/.conky in the form of windowmanager-conkyrc
#Else we will load conky without the -c parameter
#Options: true | false
LOAD_CONKY="true"
EOT
fi

if ! grep --quiet "^LOAD_XDG_AUTOSTART=" $user_config; then 
    cat <<EOT >> $user_config

#Session load xdg autostart
#This will load any application that is setup to autostart via xdg autostart directories /
#.desktop files.
#Options: true | false
LOAD_XDG_AUTOSTART="false"
EOT
fi


if ! grep --quiet "^LOAD_STARTUP_FILE=" $user_config; then 
    cat <<EOT >> $user_config

#Session load startup
#This will load any application that is setup to autostart via /etc/desktop-session/startup
#Options: true | false
LOAD_STARTUP_FILE="true"
EOT
fi

if ! grep --quiet "^LOAD_SHUTDOWN_FILE=" $user_config; then 
    cat <<EOT >> $user_config
    
#Session load shutdown
#This will run desktop-session/shutdown on logout
#Options: true | false
LOAD_SHUTDOWN_FILE="true"
EOT
fi

if ! grep --quiet "^SCREEN_BLANK_TIME=" $user_config; then 
    cat <<EOT >> $user_config

#Session screen blank
#This is to set the amount of time that the screen stays visible.
#Desktop-session sets the dpms settings according to the screen blank value set here.
#TIME expressed in seconds
#Options: 0-9...
SCREEN_BLANK_TIME="3600"
EOT
fi

if ! grep --quiet "^DBUS_SESSION_LAUNCH=" $user_config; then 
    cat <<EOT >> $user_config

#Session load of dbus-launch
#This will make it possible to start a session bus instance of dbus-daemon with the window manager
#Options: true | false
DBUS_SESSION_LAUNCH="false"
EOT
fi

if ! grep --quiet "LOG_LEVEL=" $user_config; then
    cat <<EOT >> $user_config

#Log Level
#This is where to set the level of logging done by desktop-session
#LOG_LEVEL="9" log all events
#LOG_LEVEL="5" log warnings/errors
#LOG_LEVEL="3" log errors
#LOG_LEVEL="0" do not log 
LOG_LEVEL=9
EOT
fi

if ! grep --quiet "AUTOSCALE_ENABLE=" $user_config; then
    #sed line added to avoid duplicating call to autoscale-antix as it is enabled by default in antiX startup
    #Should be removed after some sort of transition period where autoscale-antix is removed from the startup file
    sed -i "s/^[[:space:]]*autoscale-antix/# autoscale-antix/" $user_startup_file
    cat <<EOT >> $user_config

#Autoscale enable
#This is where to enable or disable the use of autoscale-antix for adjusting screen resolution and scaling
#Options: true | false
AUTOSCALE_ENABLE="true"
EOT
fi
